# ProjectPaises

Listado de Paises Con sus respectivas banderas
=============================================================
Utilizando un RecyclerView se realizo una lista de todos los paises del mundo, agregando adicionalmente la bandera, el numero de habitantes, y su respectivo himno.

Conforme se desliza la lista se presentaran cada uno de los paises que existen.

El uso de RecyclerView permite que las listas llenas de centenas de datos no se sobrecargue y mientras se va deslizando las demas van desapareciendo aparentemente. 

IMAGENES

Para las imagenes se utilizo ImageView vinculadas a su vez con imagenes subidas en internet.

VIDEO

Para el video se utilizo la vinculacion con Youtube mediante el link de cada himno

De clic en el siguiente hipervinculo para observar la imagen

[EJEMPLO DEL LISTADO DE PAISES](https://ugye-my.sharepoint.com/:i:/g/personal/kdaniela_ariasr_ug_edu_ec/EcrJ1AqDSaFEvd4vJIqLvIQBO-i98NnTCteB7_QH3x3UVg?e=WNaplG)

