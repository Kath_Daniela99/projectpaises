package com.example.CountriesProject.modelView;

public class Pais {

    private String nombre;
    private String capital;
    private String poblacion;
    private String urlBandera;
    private String urlHimno;

    public Pais(String nombre, String capital, String poblacion, String urlBandera, String urlHimno) {
        this.nombre = nombre;
        this.capital = capital;
        this.poblacion = poblacion;
        this.urlBandera = urlBandera;
        this.urlHimno = urlHimno;


    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public String getPoblacion() {
        return poblacion;
    }

    public void setPoblacion(String poblacion) {
        this.poblacion = poblacion;
    }

    public String getUrlBandera() {
        return urlBandera;
    }

    public void setUrlBandera(String urlBandera) {
        this.urlBandera = urlBandera;
    }

    public String getUrlHimno() {
        return urlHimno;
    }

    public void setUrlHimno(String urlHimno) {
        this.urlHimno = urlHimno;
    }
}


